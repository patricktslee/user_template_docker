class AddApiToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :api_key, :string
    add_column :users, :api_at, :datetime
  end
end
