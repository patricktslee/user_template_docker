class ApiUserNameController < ApplicationController


  def edit
    user = User.find_by(api_token: params[:id])
    if user 
      if user.update_attributes(name: params[:name])
        flash[:success] = t(".success")
        redirect_to user
      end
    else
      flash[:danger] = t(".danger")
      redirect_to root_url
    end
  end

end
